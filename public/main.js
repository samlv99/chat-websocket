const socket = io();

const clientsTotal = document.getElementById("clients-total");
const messageContainer = document.getElementById("message-comtainer");
const nameInput = document.getElementById("name-input");
const messageForm = document.getElementById("message-form");
const messageInput = document.getElementById("message-input");

messageForm.addEventListener("submit", function (e) {
  e.preventDefault();
  sendMessage();
});

socket.on("clients-total", (value) => {
  clientsTotal.innerHTML = `Total Clients: ${value}`;
});

function sendMessage() {
  const data = {
    name: nameInput.value,
    message: messageInput.value,
    dateTime: new Date(),
  };
  socket.emit("message", data);
  addMessageToUI(true, data);
  messageInput.value = "";
}

socket.on("chat-message", (data) => {
  addMessageToUI(false, data);
});

function addMessageToUI(isOwnMessage, data) {
  clearFeedback();
  if (data.message != "") {
    const element = `<li class=${
      isOwnMessage ? "message-left" : "message-right"
    }>
      <p class="message">
          ${data.message}
          <span>${data.name} &#0149 ${moment(data.dateTime).fromNow()}</span>
      </p>
    </li>`;

    messageContainer.innerHTML += element;
    scrollToBottom();
  }
}

function scrollToBottom() {
  messageContainer.scrollTop = messageContainer.scrollHeight;
}

messageInput.addEventListener("focus", (e) => {
  socket.emit("feedback", {
    feedback: `${nameInput.value} is typing message...`,
  });
});

messageInput.addEventListener("keypress", (e) => {
  socket.emit("feedback", {
    feedback: `${nameInput.value} is typing message...`,
  });
});

messageInput.addEventListener("blur", (e) => {
  socket.emit("feedback", {
    feedback: "",
  });
});

socket.on("feedback", (data) => {
  clearFeedback();
  const element = `
    <li class="message-feedback">
      <p class="feedback" id="feedback">${data.feedback}</p>
    </li>
  `;

  messageContainer.innerHTML += element;
});

function clearFeedback() {
  document.querySelectorAll("li.message-feedback").forEach((ele) => {
    ele.remove();
  });
}
