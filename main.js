const express = require("express");
const path = require("path");
const { Server } = require("socket.io");

const PORT = process.env.PORT || 80;
const app = express();
const server = app.listen(PORT, () => console.log(`server on port ${PORT}`));

const io = new Server(server);
app.use(express.static(path.join(__dirname, "public")));

let socketsConneted = new Set();
io.on("connection", onConnected);

function onConnected(socket) {
  console.log(socket.id);
  socketsConneted.add(socket.id);
  io.emit("clients-total", socketsConneted.size);

  socket.on("disconnect", () => {
    console.log("Socket disconnected");
    socketsConneted.delete(socket.id);
  });

  socket.on("message", (data) => {
    socket.broadcast.emit("chat-message", data);
  });

  socket.on("feedback", (data) => {
    socket.broadcast.emit("feedback", data);
  });
}
